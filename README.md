## USO DE API DE KONTROL ID

### Recibiendo datos

MÉTODO: GET

URL: https://app.kontrolid.com/api/v1/data/ID_FORMULARIO

TIPO DE AUTENTICACIÓN: Basic (datos proveídos por Kontrol ID)

### Enviando datos

MÉTODO: POST

URL: https://app.kontrolid.com/surveyKPI/upload/media

TIPO DE AUTENTICACIÓN: Basic (datos proveídos por Kontrol ID)

PARÁMETROS: 

data: imagen
