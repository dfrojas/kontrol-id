from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter(name="coordinates")
def dict_(self, name):
    dict_element = self.get(name, None)
    # return dict_element.get(name)
    return dict_element['coordinates']



