import json
import requests

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib import messages


class Home(TemplateView):
    template_name = 'home.html'


class Receive(TemplateView):
    template_name = 'data_receive.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Be careful here, s98_6866 is the key of the form that we
        # want to request, for production purposes you need to pass
        # dinamically the ID of the form. This is only for example purpose.
        call = requests.get('https://app.kontrolid.com/api/v1/data/s98_6866', auth=('api', 'api'))
        data = json.loads(call.text)
        storage = []
        for item in data:
            storage.append(item)
        context['data'] = storage
        return context


class Send(TemplateView):
    template_name = 'data_send.html'

    def post(self, request, *args, **kwargs):
        url = "https://app.kontrolid.com/surveyKPI/upload/media"
        image = request.FILES['file']
        files = image.read()
        
        r = requests.post(url, files={'data': files}, auth=('api', 'api'))

        if r.status_code == 200:
            # Just a message to the user
            messages.add_message(request, messages.INFO, 'Image successfully uploaded')
            return HttpResponseRedirect('/send')
        else:
            messages.add_message(request, messages.INFO, r.text)
            return HttpResponseRedirect('/send')
