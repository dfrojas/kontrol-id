from django.urls import path
from calls.views import Receive, Send, Home


app_name = "calls"

urlpatterns = [
	path('', Home.as_view(), name="home"),
    path('receive/', Receive.as_view(), name="receive"),
    path('send/', Send.as_view(), name="send"),
]
